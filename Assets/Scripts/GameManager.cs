using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class GameManager : MonoBehaviour
{

    [SerializeField] private List<GameObject> Nodes;
    [SerializeField] private List<GameObject> Units;
    [SerializeField] private Unit ActiveUnit, EnemyUnit;
    [SerializeField] private Node_Script ActiveNode;
    [SerializeField] private UI_Controller UI_Controller_Script;
    [SerializeField] private bool Player_1_Turn;
    [SerializeField] private int winPointCounter, WinPointTarget, RoundCounter, RoundTarget;
    

    private void Awake()
    {
        Nodes = GameObject.FindGameObjectsWithTag("Node").ToList();
        Units = GameObject.FindGameObjectsWithTag("Unit").ToList();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Player_1_Turn)
        {
            UI_Controller_Script.SetTurnDisplayText("Red Turn");
        }
        else
        {
            UI_Controller_Script.SetTurnDisplayText("Blue Turn");
        }

        if (winPointCounter >= WinPointTarget)
        {
            UI_Controller_Script.WinGame();
        }
        if (RoundCounter >= RoundTarget)
        {
            UI_Controller_Script.LoseGame();
        }

        

    }

    public void UpdateActiveUnit()
    {
        string tempName;
        foreach (var unit in Units)
        {
            tempName = unit.GetComponent<Unit>().GetName() + " Strengh: " + unit.GetComponent<Unit>().calcStrengh();
            print(tempName);
            if (tempName.Equals(UI_Controller_Script.GetDropDownString()))
            {
                ActiveUnit = unit.GetComponent<Unit>();
            }
        }

    }

    public void UpdateActiveNode()
    {
        ActiveNode = Nodes[getIndex(UI_Controller_Script.GetTerritoryDropDownMenuValue())].GetComponent<Node_Script>();
    }

    public void UpdateActive()
    {
        UpdateActiveUnit();
        UpdateActiveNode();
    }

    public List<GameObject> getUnits()
    {
        return Units;

    }

    public void Move()
    {
        ActiveUnit.SetActiveNode(ActiveNode);
        ActiveUnit.MoveUnits();
        Player_1_Turn = !Player_1_Turn;
        UI_Controller_Script.DropdownValueChanged();
        UpdateActive();
        UI_Controller_Script.Reset_UI();
        RoundCounter++;
    }

    public int getIndex(string NodeName)
    {
        string tempName;
        for (int i = 0; i < Nodes.Count; i++)
        {
            tempName = Nodes[i].GetComponent<Node_Script>().GetName();
            if (tempName.Equals(NodeName))
            {
                return i;
            }
        }
        return 5;
    }

    public void Attack()
    {
        if (ActiveNode.GetIsOccupied())
        {
            foreach (var option in Units)
            {
                if (option.GetComponent<Unit>().GetActiveNode().Equals(ActiveNode))
                {
                    EnemyUnit = option.GetComponent<Unit>();
                    break;
                }
            }
            if (ActiveUnit.calcStrengh() <= EnemyUnit.calcStrengh())
            {
                Units.Remove(ActiveUnit.gameObject);
                ActiveUnit.UnitLost();
            }
            else
            {
                if (ActiveNode.GetISWinPoint())
                {

                    if (ActiveUnit.Get_Player_1())
                    {
                        winPointCounter++;
                    }
                    else
                    {
                        winPointCounter--;
                    }
                }
                
                Units.Remove(EnemyUnit.gameObject);
                EnemyUnit.UnitLost();
            }
        }
        Player_1_Turn = !Player_1_Turn;
        UI_Controller_Script.DropdownValueChanged();
        UpdateActive();
        UI_Controller_Script.Reset_UI();
        RoundCounter++;
    }

    public bool GetPlayer_1_Turn()
    {
        return Player_1_Turn;
    }

    public Unit getActiveUnit()
    {
        return ActiveUnit;
    }

    public int GetDefenceStrengh(Node_Script Node)
    {
        int valueToReturn = 0;
        foreach (var item in Units)
        {
            if (item.GetComponent<Unit>().GetActiveNode().GetName().Equals(Node.GetName()))
            {
                valueToReturn = item.GetComponent<Unit>().calcStrengh();
                break;
            }
        }
        return valueToReturn;
    }
}

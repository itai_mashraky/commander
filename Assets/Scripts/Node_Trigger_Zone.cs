using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node_Trigger_Zone : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Node"))
        {
            GetComponentInParent<Node_Script>().AddNodeInRange(other.gameObject);
        }
    }

}

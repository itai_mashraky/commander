using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankUnit : Unit
{
    [SerializeField] private int armorPoint;


    // Start is called before the first frame update
    void Start()
    {
        armorPoint = 20;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override int calcStrengh()
    {
        return 0;
    }

    public override void TakeDamage(int damage) //5
    {
        if(armorPoint > 0) // 3
        {
            armorPoint = armorPoint - damage; // -2
            damage += armorPoint; // 3
        }
        if (damage >= 0)
        {
            this.UnitCounter -= damage;
        }
    }


    public override void AddUnits(int amount)
    {
        throw new System.NotImplementedException();
    }

}

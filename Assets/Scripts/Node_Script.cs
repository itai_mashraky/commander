using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Node_Script : MonoBehaviour
{
    [SerializeField] private string NodeName;
    [SerializeField] private List<GameObject> NodesInRangeList;
    [SerializeField] private bool IsPlayer, IsOccupied, IsWater, IsWinPoint;
    

    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public List<GameObject> GetNodesInRangeList()
    {
        return NodesInRangeList;
    }

    public void AddNodeInRange(GameObject temp)
    {
        if (this.IsWater)
        {
            if (temp.GetComponent<Node_Script>().getIsWater())
            {
                NodesInRangeList.Add(temp);
            }
        }
        else
        {
            if (!temp.GetComponent<Node_Script>().getIsWater())
            {
                NodesInRangeList.Add(temp);
            }
        }
    }
    public bool GetIsPlayer()
    {
        return IsPlayer;
    }
    public void SetIsPlayer(bool Value)
    {
        this.IsPlayer = Value;
    }
    public bool GetIsOccupied()
    {
        return IsOccupied;
    }
    public void SetIsOccupied(bool value)
    {
        this.IsOccupied = value;
    }
    public override bool Equals(object other)
    {
        Node_Script temp = (Node_Script)other;
        return this.NodeName.Equals(temp.NodeName);

    }
    public bool getIsWater()
    {
        return this.IsWater;
    }
    public bool GetISWinPoint()
    {
        return this.IsWinPoint;
    }
    public string GetName()
    {
        return this.NodeName;
    }
}

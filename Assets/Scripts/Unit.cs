using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Unit : MonoBehaviour
{
    [SerializeField] protected string UnitName;
    [SerializeField] protected int UnitCounter;
    [SerializeField] protected int UnitStrengh;
    [SerializeField] protected int StrenghMultyplier;
    [SerializeField] protected GameObject UnitPreFab;
    [SerializeField] protected GameObject ActiveUnitsPreFab;
    [SerializeField] protected Node_Script ActiveNode;
    [SerializeField] protected Vector3 posOffset;
    [SerializeField] protected bool Player_1;

        
    public abstract int calcStrengh();

    public abstract void TakeDamage(int damage);

    public virtual void MoveUnits()
    {
        ActiveUnitsPreFab.transform.position = ActiveNode.transform.position + posOffset;
    }

    public virtual void SetActiveNode(Node_Script Node_)
    {
        this.ActiveNode.SetIsOccupied(false);
        this.ActiveNode = Node_;
        ActiveNode.SetIsOccupied(true);
        if (Player_1)
        {
            ActiveNode.SetIsPlayer(true);
        }
        else
        {
            ActiveNode.SetIsPlayer(false);
        }
    }

    public abstract void AddUnits(int amount);

    public virtual void SpawnUnits()
    {
        ActiveNode.SetIsOccupied(true);
        if (Player_1)
        {
            ActiveNode.SetIsPlayer(true);
        }
        else
        {
            ActiveNode.SetIsPlayer(false);
        }
        ActiveUnitsPreFab = Instantiate(UnitPreFab, ActiveNode.transform.position + posOffset, Quaternion.identity);

    }
    public bool Get_Player_1()
    {
        return Player_1;
    }

    public virtual Node_Script GetActiveNode()
    {
        return ActiveNode;
    }

    public void UnitLost()
    {
        ActiveNode.SetIsOccupied(false);
        Destroy(ActiveUnitsPreFab);
        Destroy(this.gameObject);
    }

    public string GetName()
    {
        return this.UnitName;
    }
}
